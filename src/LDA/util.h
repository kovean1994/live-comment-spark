#include <stdio.h>
#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>

double log_sum(double log_a, double log_b);
double trigamma(double x);
double log_gamma(double x);
int argmax(double* x, int n);
double digamma(double x);

double opt_alpha(double ss, int D, int K);


