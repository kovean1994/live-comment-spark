LOS ANGELES — From the ninth floor of a downtown office building on Wilshire Boulevard, Jack Soussana delivered staggering numbers of mortgages to homeowners during the real estate boom, amassing a fortune.
Skip to next paragraph
Enlarge This Image
Nanine Hartzenbusch for The New York Times

FORECLOSED, AND WAITING FOR THE NEXT STEP Joshua Garland and his wife, Jaimee, at their home in Charlotte, N.C.
Back to Business
Same Cast, New Roles

This article is the fourth in a series examining the battles to reshape the financial industry.
Previous Articles in the Series »
Multimedia
Dissatisfied CustomersGraphic
Dissatisfied Customers
Related
Times Topics: Credit Crisis — The Essentials
Readers' Comments

    Readers shared their thoughts on this article.

    * Read All Comments (164) »

By Mr. Soussana’s own account, his customers fared less happily. He specialized in the exotic mortgages that have proved most prone to sliding into foreclosure, leaving many now scrambling to save their homes.

Yet the dangers assailing Mr. Soussana’s clients have yielded fresh business for him: Late last year, he and his team — ensconced in the same office where they used to broker mortgages — began working for a loan modification company. For fees reaching $3,495, with most of the money collected upfront, they promised to negotiate with lenders to lower payments on the now-delinquent mortgages they and their counterparts had sprinkled liberally across Southern California.

“We just changed the script and changed the product we were selling,” said Mr. Soussana, who ran the Los Angeles sales office of Federal Loan Modification Law Center. The new script: You got a raw deal, and “Now, we’re able to help you out because we understand your lender.”

Mr. Soussana’s partners at FedMod, as the company is known, were also products of the formerly lucrative world of high-risk lending. The managing partner, Nabile Anz, known as Bill, previously co-owned Mortgage Link, a California subprime lender, now defunct, that once sold $30 million worth of loans a month.

Jeffrey Broughton, one of FedMod’s initial partners, served as director of business development at Pacific First Mortgage, a lender that extended so-called Alt-A mortgages for borrowers with tarnished credit for Countrywide Financial, which lost billions of dollars on bad mortgages before being rescued in an acquisition.

FedMod is but one example of how many of the same people who dispensed risky mortgages during the real estate bubble have reconstituted themselves into a new industry focused on selling loan modifications.

Despite making promises of relief to homeowners desperate to keep their homes, FedMod and other profit making loan modification firms often fail to deliver, according to a New York Times investigation based on interviews with scores of former employees and customers, more than 650 complaints filed with the Better Business Bureau, and documents filed by the Federal Trade Commission in a lawsuit against the company.

The suit, filed in California federal court, asserts that FedMod frequently exaggerated its rates of success, advised clients to stop making their mortgage payments, did little or nothing to modify loans and failed to promptly refund fees. The suit seeks an end to FedMod’s practices, and compensation for customers.

“Our job was to get the money in and then we’re done,” said Paul Pejman, a former sales agent who worked out of FedMod’s two-story headquarters in Irvine, Calif. He recounted his experience, he said, because “I really feel bad.”

“I had people calling me crying, and we were telling them, ‘You can pay me or you can lose your house,’ ” Mr. Pejman said. “People were giving me every dime they had, opening credit cards. But I never saw one client come out of it with a successful loan modification.”

Mr. Anz, who is challenging the F.T.C. lawsuit, acknowledged that FedMod’s business went “horribly wrong,” but he maintains the company made genuine efforts to help delinquent borrowers. He said FedMod has refunded fees to 3,000 dissatisfied customers, while modifying 1,500 mortgages.

A New Mission

FedMod is among dozens of similar companies that have been accused by state and federal authorities of fraudulent business practices. On the same day in April that the F.T.C. sued FedMod, it brought action against four similar companies and sent letters of warning to 71 others. Last week, the commission brought lawsuits against four more loan modification companies, advancing an enforcement campaign involving 23 states.

Many of the companies formerly operated as mortgage brokers, The Times found. Since October, the California Department of Real Estate has ordered 210 businesses and individuals to stop offering loan modification or foreclosure prevention services, because they lacked a real estate license, as required by the state. In fact, nearly half the people have roots in the mortgage industry or other areas of real estate, according to public records.

Debt Barter Inc. is among them. A loan modification company based in Irvine that was cited by the state in January for collecting upfront fees without a license, it is owned by Sean R. Roberts, who formerly headed Instafi, a mortgage broker that closed $2 billion worth of loans a year at its peak. Since February, customers have filed 17 complaints against Debt Barter with the Better Business Bureau. Most accused the company of charging upfront fees, then failing to lower their payments.

“We can’t please everyone all the time,” said Mr. Roberts, who added that the company had modified loans for nearly 300 of its roughly 500 clients. 
