VERBIER, Switzerland — With six stages left in the Tour de France and some of the toughest stages yet to come, Andy and Frank Schleck — brothers on the Saxo Bank team — insist that there is still hope for one of them to win this race.
Skip to next paragraph
Multimedia
The Tour Stage by StageInteractive Map
The Tour Stage by Stage
Related
With a Late Climb, Contador Controls the Tour de France (July 20, 2009)
A Day Later, Hincapie and Cavendish Still Feel Cheated (July 20, 2009)

“When we were kids, we always dreamed it would happen, and now we’re going to do whatever we can to finally make it real,” Frank Schleck, the older of the two, said. “It gives me goose bumps just to think about us doing sprints down the street when I was 13 or 14 and Andy was, like, 10. We would raise our arms when we won.”

Standing between the Schlecks and the yellow jersey, though, are several other riders, including the race’s overall leader, Alberto Contador, who took the lead Sunday with a spectacular solo breakaway to the base of a ski resort here.

Lance Armstrong, a seven-time Tour winner, is second, 1 minute 37 seconds back. Bradley Wiggins of Garmin-Slipstream is third, 1:46 back.

The Schlecks are even farther down the leader board, but Contador said Monday that Andy Schleck was his biggest rival going into the Tour’s final days.

Andy Schleck, 24, is in fifth, 2:26 back. Frank Schleck, 29, is 10th, 3:25 back. On Monday, those brothers and their Saxo Bank team went out of their way to point out that this three-week race was far from over. Even more important, they said, is that history has shown Contador to be breakable. “Physically, Contador will be difficult to beat right now, but what happened in Paris-Nice could happen again,” Saxo Bank’s team director, Bjarne Riis, said. “If you can isolate him and stress him, then he just might be beatable. We will see.”

At this year’s Paris-Nice race, in March, Contador ran out of energy in one stage and lost the lead. He said he had forgotten to eat and drink as other riders attacked him and left him isolated from his teammates.

Afterward, Armstrong wrote on his Twitter page that Contador was talented but had “a lot to learn.” But Armstrong, who could not keep up with Contador in either mountain stage of this Tour, has vowed to help Contador, his Astana teammate, win this race.

There is a time trial left, however, which would give Armstrong an opportunity to make up time on Contador. And there is the possibility that Contador will falter, which is what the other teams are hoping for.

“No matter what he says or what the situation seems like, Lance is Lance, and that means you should never count him out,” said Rolf Aldag, the director of the Columbia-HTC team.

“But now I see the only teams that could beat Astana and Contador are Saxo Bank and Garmin, the ones with more than one rider in the G.C.,” Aldag said, referring to the top riders in the general classification, which is the overall ranking.

“You need those numbers because Astana has Contador and three other guys who are very strong. Other teams need to do something — and pretty soon — if they want to keep Astana from winning.”

The Schlecks, from Luxembourg, said they would work together to get the job done. They have the credentials to do it, too.

Frank Schleck wore the yellow jersey here for two days in 2008, when he finished sixth. Andy Schleck won this year’s Liège-Bastogne-Liège race and finished second in last year’s Giro d’Italia.

Off their bikes, the two are like typical siblings, their teammate Jens Voigt said, adding that their relationship “is like a daily soap opera.”

Voight said: “Frank is trying to act like the big brother all the time, saying: ‘Oh, Andy. Oh, Andy, you have to do things this way.’ And Andy is saying, ‘Frankie, oh Frankie, what do you want me to do?’ But on the bike, there is no friction between them and they are stronger together than they are apart.”

In other stages, Frank Schleck said he had tried to hold his younger brother back. But on Sunday, Andy Schleck zoomed off after Contador anyway in Stage 15, chasing Contador up the punishing incline by himself as he tried to bridge the gap.

Andy Schleck said he had the confidence to catch Contador in this race and steal the yellow jersey from him.

“We have the same physical condition, so over the next few days, we will see who is stronger,” he said. “We really want to beat him and we are ready to suffer for it. We will try until we die.”

From now until the race ends on July 26, riders in the peloton are more likely to suffer than not, as some of the Tour’s hardest stages challenge them. In Stage 16 on Tuesday, there are two climbs called the two St. Bernards. The smaller one is a Category 1 climb, or the toughest categorized climb. The bigger is beyond categorization, meaning that it is the most grueling climb out there.

In Wednesday’s Stage 17, the riders face five climbs, including four Category 1s. After that is a 24.9-mile time trial around Lake Annecy, followed by a stage with undulating roads.

All that leads to Saturday, which is the daunting climb of Mount Ventoux, the bald, windy mountain on which top riders could lose several minutes, or more, in the overall standings.

The Schleck brothers say that by then they will have made their move, just as they practiced when they were younger.

Frank Schleck recalled a family vacation that was canceled because his father, Johnny, had bought three new bikes.

Johnny Schleck, a former professional rider, wanted to stay home and test out the new equipment. Again, the brothers pretended that they were racing in the Tour de France.

“Now we are here, racing with Lance Armstrong and Alberto Contador in the race that we used to watch on TV, in the Tour de France, where we used to pretend to celebrate stage wins,” Frank Schleck said.

“This whole thing is just a dream that we’ve acted out again and again.”
