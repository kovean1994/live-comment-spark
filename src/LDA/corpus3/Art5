His former students will tell you that Frank McCourt, who died Sunday, was too attuned to the false note ever to declare, once he had become a huge success as an author, that he missed teaching high school. Even so, he spent three decades as a teacher of English and creative writing in New York City’s public schools. And he was the first to say that those years, while depriving him of the time actually to write, were what made a writer out of him. He had long been retired by 1996, when his first book, “Angela’s Ashes,” was published.
Skip to next paragraph
Eric Feferberg/Agence France-Presse — Getty Images

Frank McCourt wrote “Angela’s Ashes” and other works.
Related
From ‘Angela’s Ashes,’ a Communion Story (July 20, 2009)
Frank McCourt, Whose Irish Childhood Illuminated His Prose, Is Dead at 78 (July 20, 2009)
Times Topics: Frank McCourt

Review: ‘Angela’s Ashes’ (September 17, 1996)
ArtsBeat: Share Your Memories of Frank McCourt

 The Takeaway: Motoko Rich on Frank McCourt
Enlarge This Image
Keith Meyers/The New York Times

Frank McCourt, left, with his brother Malachy in 1984 at the Village Gate.

Mr. McCourt began teaching in 1958, when he was 28, at Ralph R. McKee Vocational High School on Staten Island, and from 1972 to 1987 taught at Stuyvesant High School, a highly selective school, then on East 15th Street in Manhattan. His students learned from him that literature was nothing more — and nothing less — than the telling of stories.

Of course he made his students dip into the canon; they learned to write from reading Swift, Joyce, Hawthorne, Hemingway and Flannery O’Connor. But, as many of them have said, the most inspired and inspiring hours spent in his classroom were devoted to listening to him share experiences from his own life.

“A lot of the class was him telling tales and telling them over and over,” said Alissa Quart, an author and a 2009 Nieman Fellow at Harvard who was Mr. McCourt’s student during her freshman year at Stuyvesant, in 1985-86. “He used to sort of recite from memory the stories that became ‘Angela’s Ashes.’ ”

The book, an international best seller many times over and winner of the Pulitzer Prize, chronicled Mr. McCourt’s sad, impoverished childhood in Ireland.

As the news of Mr. McCourt’s death spread, hundreds of admirers, including many former students, posted their recollections on nytimes.com. They talked about his influence as a teacher, meeting him or hearing him read, and the joy that his books had delivered.

“Frank was the Lou Reed of high school English, sending writers, chroniclers and those with memory out into the world aware and ready to savor experience for its own sake, long before he ever took pen to paper to compose his now famous trilogy,” one commenter who identified himself as a Stuyvesant alumnus wrote, referring to the old line that the Velvet Underground’s first album sold 10,000 copies, and started as many bands.

A former student, Kwana Jackson, now a writer of romance novels, provided evidence to support this. She called Mr. McCourt “one my personal heroes,” and linked to her blog, where she wrote of his class: “It was where I started to really love the written word and started to crave the writer’s life. Only Mr. McCourt could make suffering desirable. Hell, you were going to suffer in this life anyway you might as well do it doing something you love.”

Dan Coleman, who studied with Mr. McCourt at Stuyvesant and returned to teach Mr. McCourt’s writing course during the 1990s, said students also heard stories less tragic than those from his childhood about Mr. McCourt’s life as a single man. (He was married three times.) “He would come in and tell us, in his beautiful brogue, charming, hilarious stories about how he tried to play off of the maternal instincts of the women he’d meet — making reference to how much laundry he had that needed to be done, things like that,” Mr. Coleman said.

“Looking back, it was all part of a technique,” said Vernon Silver, Stuyvesant class of 1987 and a reporter for Bloomberg News in Rome whose book “The Lost Chalice” has just been published. “He wanted you to tell a story too.”

A common exercise was asking students to describe what they had done when they got home the night before. “He would coax it out of us, showing us how to pay attention to mundane but telling details,” Mr. Silver said. “I remember a dialogue with a shy student. The kid said, ‘I did my homework.’ McCourt said: ‘No, no, no. What did you do when you walked in? You went through a door, didn’t you? Did you have anything in your hands? A book bag? You didn’t carry it with you all night, did you? Did you hang it on a hook? Did you throw it across the room and your mom yelled at you for it?’ ”

And on and on, until enough significant glimpses of the boy’s life emerged to begin to paint a picture. In “Teacher Man” Mr. McCourt wrote that he came upon his method by accident on his second day at McKee. A joke he made about relations with sheep as a boy in Ireland did not go over well with his colleagues:

“In the teachers’ cafeteria veterans warned me, Son, tell ’em nothing about yourself. ...You’re the teacher. You have a right to privacy. The little buggers are diabolical. They are not, repeat not, your natural friends. ... You can never get back the bits and pieces of your life that stick in their little heads. Your life, man. It’s all you have.”

He went on: “The advice was wasted. ... My life saved my life.”

There was more to it than that. “Frank had us sing salacious folk songs, he had us write courtroom defenses of inanimate objects and recite recipes as poetry,” said Susan Jane Gilman, a former student who has published two memoirs. “Stuyvesant was largely for math-science types, it was learning by rote. Frank’s class was an intellectual freefall. I looked forward to it every day.”

Although his books were still years off, Mr. McCourt was famous at Stuyvesant throughout much of his time there. “If you were at the school and you wanted to write, you went to meet McCourt,” said David Lipsky, the author, most recently, of “Absolutely American,” a book about West Point. “It wasn’t ‘go read the complete works of J. D. Salinger.’ It was one word: McCourt.”

Many of his former students became writers, and many kept in touch with him. Ms. Gilman said: “We all thought, ‘He’s such a genius, what’s he doing just teaching us?’ Everybody thought he was destined for bigger and better things. And when he became a global phenomenon, we felt it was justice.”
