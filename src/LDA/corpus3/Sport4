LAS VEGAS — The first Israeli in the N.B.A., Omri Casspi, is busily trying to adapt to life in the United States.
Skip to next paragraph
N.B.A.

    * Live Scoreboard
    * Results and Schedule
    * Teams | Statistics

Knicks

    * Schedule/Results
    * Roster | Statistics

Nets

    * Schedule/Results
    * Roster | Statistics

Jack Guez/Agence France-Presse — Getty Images

Omri Casspi
Enlarge This Image
Justin Lane/European Pressphoto Agency

Fans at Madison Square Garden last month when the Kings took Casspi with the 23rd pick.

For starters, he needs a cellphone with a local number. He just received a $4,500 bill for about two weeks of calls, which is expensive even by N.B.A. standards. He needs new chargers for all his gadgets. But he is struggling most to find comfort food.

“Hummus,” Casspi said, with a hard h and a long u, stressing the first syllable in a way that conveyed utter seriousness. “You don’t have that here, though.”

A reporter insisted that the chickpea spread is widely available in grocery stores in the United States, but Casspi — who was drafted last month by the Sacramento Kings — smiled dismissively.

“Man, I tried it; that’s all I can say,” he said last week during a break in the Kings’ summer league schedule. “I will bring some from Israel, maybe. I’ll let you taste it and you tell me.”

It seems that a bulk order from the Tel Aviv equivalent of Costco may be necessary.

Then there are weightier adjustments, like getting used to being the greatest hope in Israeli basketball history.

No Israeli has ever played in the N.B.A. Until last month, none had ever been drafted in the first round.

When the Kings took Casspi with the 23rd pick, he became the first Israeli to secure a guaranteed contract, which will almost assuredly make him the first to play in an N.B.A. game.

That moment will come this fall. The celebrations began immediately on draft night.

“It was a huge festival in Israel,” said Dan Shamir, a longtime Israeli coach who worked with Casspi when Casspi was a teenager on the national team. “For many years, people were asking when Israel will have an N.B.A. player. When it actually happened, it made huge headlines.”

At home in Yavne, a suburb of Tel Aviv, the 21-year-old Casspi celebrated with friends and family, and wept. The emotions were overwhelming, not only because Casspi had attained a goal, but also because he had realized a nation’s dream.

Basketball is the No. 2 sport in Israel, trailing only soccer. Maccabi Tel Aviv, Casspi’s team, is considered a national treasure, with 47 championships and 5 European Cups.

“Iconic,” Shamir, who now coaches Bnei Hasharon, said of Maccabi. “Players there are like rock stars in Israel.”

In the days before satellite television and 500 channels, the country would practically shut down on Thursday nights, when Maccabi was on TV. “The streets were empty,” Shamir said.

Next fall, thousands of Israeli alarm clocks may simultaneously wail around 4 in the morning, when N.B.A. games are broadcast. The Kings may become the second-most-popular basketball team in Israel. And Casspi will become an instant hero to millions of Israelis and Jews worldwide.

He is embracing the honor, though with no small measure of anxiety.

“I think all the eyes and ears in Israel, in basketball in Israel, are focused on me now,” he said, sitting behind a desk in his hotel room in Las Vegas. “There is big expectations, and all the Jewish community in the States is really excited about it. So I think there’s a big responsibility with it.”

As a child, Casspi said, he would wake up early in the morning to watch N.B.A. games, particularly the Chicago Bulls. He was a huge Michael Jordan fan. But Casspi’s dream then, he said, was to play for Maccabi “because I had nobody to look up to in the N.B.A., no Israeli.”

He added, “I think the young kids right now, they have somebody to look up to.”

There was no egotism in his tone, just a statement of fact. Casspi hastened to add: “I’m just trying to focus on basketball. I’m not trying to think about all that.”

There is much work to do on the court. The Kings had the N.B.A.’s worst record last season, 17-65, and they have not made the playoffs since 2006.

A 6-foot-9, 225-pound forward, Casspi is part of a promising but raw core that also includes the fourth pick in the draft, guard Tyreke Evans. They are joining a youthful roster featuring guard Kevin Martin and center Spencer Hawes.

Casspi, too, is a work in progress. He is a capable shooter, rebounder and ball handler, but he does not excel in any one area and is not considered particularly athletic. He said his goal was to become another Hedo Turkoglu, the playmaking forward from Turkey who helped guide Orlando to the N.B.A. finals.

Jason Levien, the Kings’ assistant general manager, said the franchise was drawn to Casspi for his passion, toughness and tenacity. The word energy comes up often.

“That’s his specialty,” said David Thorpe, a coach at IMG Academies and an analyst for ESPN, who watched Casspi during the summer league. “Energy in the N.B.A. is a real talent.”

Casspi shot 33 percent in his first four summer league games, averaging 7.3 points, 3.3 rebounds and 4 turnovers. He is still trying to regain his rhythm after the whirlwind draft process and a brief holdup in paperwork that kept him from practicing with the team.

Even with Maccabi, Casspi’s value was never evident in the raw statistics but in the results.

“He’s fearless,” Shamir said. “When he was on the floor, things happened for them.”

Before Casspi was drafted, The Jerusalem Post documented the many Israeli players who just missed making it to the N.B.A. In 1979, Miki Berkowitz was set to join the Atlanta Hawks, but Maccabi would not release him from his contract. Twenty years later, Oded Katash appeared close to joining the Knicks, but the N.B.A. lockout and contractual issues killed the deal.

The Israelis Lior Eliyahu and Yotam Halperin were taken in the 2006 draft. But as second-round picks, they did not have guaranteed contracts and thus had little incentive to make the move.

More than 60 other nations have been represented in the N.B.A., including Belize, Estonia and Denmark. The Middle East has produced players from Egypt and Lebanon. The first Iranian, Hamed Haddadi, arrived last season.

Casspi’s arrival has energized Jews in Sacramento. A large contingent — many wearing jerseys with Casspi’s name spelled in Hebrew — turned out for a postdraft rally. They have taken Casspi on tours of the city and offered assistance in finding a house, a car and, naturally, some good restaurants.

“I feel blessed, really, to be in this situation,” Casspi said.

He is, however, still searching for a good Israeli restaurant and a worthy tub of hummus.
