Over the last few years reality-show casting calls have become almost as much of a cultural commonplace as the shows themselves — the familiar scenes of hundreds of anxious strangers converging on a street corner with their résumés, their headshots and their A games, hoping for some kind of immortality or at least a more interesting career.
Skip to next paragraph
Related
'Artstar' on Gallery HD: The Art World Tries Realism (the TV Kind) (May 28, 2006)
Blog
ArtsBeat
ArtsBeat

The latest on the arts, coverage of live events, critical reviews, multimedia extravaganzas and much more. Join the discussion.

    * More Arts News

Enlarge This Image
Todd Heisler/The New York Times

Jesse Edwards, right, of Seattle organizes his paintings while waiting in line in the West Village to audition for a reality show.

But few such casting calls have looked like the one that began in the wee hours of Saturday morning in the West Village, where Jeff Lipsky, a 37-year-old painter and digital artist from Tyngsboro, Mass., unfolded his New England Patriots lawn chair and camped out for the night in front of the White Columns gallery, first in line to audition for a new reality show being created for Bravo. Produced by Sarah Jessica Parker, the show, which doesn’t have a title or a broadcast date, will try to do for the contemporary art world what the cable channel has done for the worlds of fine cuisine (“Top Chef”) and fashion (“Project Runway”): discover young, or maybe even middle-aged or old, unknowns with the talent to command the attention of both a television audience and a serious audience in the creative field to which they aspire.

The 13 finalists eventually chosen — from among hundreds who have already auditioned in Los Angeles, Miami, Chicago and now in New York — will compete for a gallery show, a cash prize and a sponsored national museum tour, though the producers have not revealed how much money is at stake or which museums or galleries will participate.

It seemed to matter little to the 150 or so artists who had already gathered by 8:30 Saturday morning, bearing all manner of art: a ghoulish portrait of a face that appeared to be Michael Jackson’s melded with Elvis’s; a crazily beaded mannequin torso with the sparkly word “GIRL” attached like a tiara to the top of its head; a Caravaggio-esque painting of St. Sebastian, skewered and suffering; a photo-realistic canvas so large it arrived on a truck. At the corner of Horatio and Hudson Streets one artist was slowing traffic considerably as he applied bright blue swirly paint to the body of a topless woman who was wearing only a flesh-colored thong.

Second in line, after arriving at 1 in the morning on a flight from Fort Myers, Fla., was Jeffrey Scott Lewis, a 48-year-old collagist, single father and former store-window designer who brought along a colorful, mosaiclike work he had made from gum wrappers. (He quit smoking in February and described gum as his “new best friend.”)

“I’ve wanted to be on a reality show since the first time I saw ‘Survivor’ — but without the bug bites and stuff,” Mr. Lewis said.

“I got here about 2:30 in the morning, and the only thing I saw was this empty chair,” he said, pointing to Mr. Lipsky’s lawn chair. “And I got a little spooked, so I walked around the neighborhood for a while before I came and got in line.”

Nick Gilhool, a casting director for Magical Elves, the production company that created “Top Chef” and “Project Runway” and is helping produce the art series, said that judges and casting officials had seen a remarkably wide range of artists, from “hobbyist Sunday-painter types” to 20-somethings just out of art school to older artists who had met with some success but whose careers had languished for one reason or another. (One artist at the Miami audition flew in from Thailand.)

He declined to reveal the identity of the judges, though he described them as curators, artists, dealers, teachers and collectors “whose names people in the art world would certainly recognize.” The lone judge brought out for interviews was Simon de Pury, chairman of the auction house Phillips de Pury. He said that he did not hesitate when asked to become involved, and that his hope for the program was that it would help penetrate the air of “hermetic inapproachability” surrounding contemporary art.

Mr. Gilhool said the main criterion in picking artists was to create a show that “people in the art world will want to tune into every week to actually see the work.” But he added that the fragmented and raucous nature of contemporary art would probably make it trickier to produce than competitions dealing with more straight-ahead creations like food or clothing design. What would be the equivalent, for example, of a “quick-fire challenge,” the part of “Top Chef” in which cooks have to whip up a dish lightning fast? Life drawing with a stopwatch? Found-art scavenger-hunt race? Best postironic conceptual gambit in under a minute?

“I think there’s a reason why this really hasn’t been done before: because there are a lot of pitfalls,” Mr. Gilhool said. (In 2006 Gallery HD, a now-defunct high-definition channel, broadcast “Artstar,” an eight-episode reality show in which contestants produced works for a group exhibition at Deitch Projects, the SoHo gallery. One of its finalists, Virgil Wong, a New York conceptual artist, was in line Saturday to try out for Bravo’s show.)

By the end of Saturday’s cattle call almost 400 hopefuls had turned up. About a third of the way back in the line, Jesse Edwards, a 31-year-old painter and ceramics artist from Seattle who has been living hand to mouth since moving to New York this summer, opened his portfolio to show a picture of a work that the producers might keep handy as a cautionary reminder: a ceramic television with an image of painted apples as its screen. The piece was titled “Still Life Channel.”

“It’s a snoozer of a channel, the Still Life Channel,” Mr. Edwards said, but then quickly showed a picture of another ceramic television, this one with a mirror as its screen, titled “Your Personal Moment of Fame.”

“That channel can be whatever you want it to be,” he said. “It can be great. It’s all up to you.”
