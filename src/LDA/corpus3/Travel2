WITH the quiet momentum of a work in progress, Cincinnati is finding an artsy swagger, infused with a casual combination of Midwest and Southern charm. The city center, for decades rich with cultural and performing arts venues, now offers a renovated Fountain Square area and a gleaming new baseball stadium with views of the Ohio River. Efforts to transcend the damage from several days of race riots in 2001, which nearly decimated the city’s Over-the-Rhine district, are slowly progressing. Transformations are taking place in surrounding areas — as well as across the river in the neighboring Kentucky cities of Newport and Covington — with their cool music venues, funky shopping outlets and smart culinary options.
Skip to next paragraph
Cincinnati Travel Guide
Where to Stay
Where to Eat
What to Do
Go to the Cincinnati Travel Guide »
Multimedia
CincinnatiMap
Cincinnati
A Weekend in CincinnatiSlide Show
A Weekend in Cincinnati

Friday

4 p.m.
1) TRANQUILLITY AND ETERNITY

A graveyard may not be the most obvious place to start a trip, but Spring Grove Cemetery and Arboretum (4521 Spring Grove Avenue; 513-681-7526; (www.springgrove.org/sg/arboretum/arboretum.shtm) is not your average resting place. The arboretum, designed in 1845 as a place for botanical experiments, features 1,200 types of plants artfully arranged around mausoleums and tranquil ponds. Roman- and Greek-inspired monuments bear the names of many of Cincinnati’s most prominent families — Procter, Gamble and Kroger included. Admission and parking are free, and the office provides printed guides and information about the plant collection.

6 p.m.
2) WHERE HIPSTERS ROAM

The Northside district has recently blossomed into a casually hip destination for shopping and night life, particularly along Hamilton Avenue. Vinyl gets ample real estate at Shake It Records (4156 Hamilton Avenue; 513-591-0123; www.shakeitrecords.com), a music store specializing in independent labels; if you can’t find a title among the 40,000 they carry, the owners will track it down for you. For a bite, locals swear by Melt (4165 Hamilton Avenue; 513-681-6358; www.meltnorthside.com), a quirky restaurant friendly to vegans and carnivores alike. Order the Joan of Arc sandwich ($8.45), with blue cheese and caramelized onions atop roast beef, or the hummus-laden Helen of Troy ($6.95), and retreat to the garden.

9 p.m.
3) LOCAL BANDS, LOCAL BEER

Saunter next door to find 20-somethings in skinny jeans mingling with 30-somethings in flip-flops at Northside Tavern (4163 Hamilton Avenue; 513-542-3603; www.northside-tavern.com), the area’s best spot for live music. Sip a pint of Cincinnati’s own Christian Moerlein beer ($3.50) and listen to jazz, blues and acoustic rock acts in the intimate front bar, or head to the back room, where the best local bands take the larger stage. Wind down with a crowd heavy with artists and musicians at the Comet (4579 Hamilton Avenue; 513-541-8900; www.cometbar.com), a noirish dive bar with an impossibly cool selection on its jukebox and top-notch burritos ($5) to satisfy any late-night cravings.

Saturday

9:30 a.m.
4) THE AEROBIC ARABESQUE

The fiberglass pigs in tutus that greet you outside the Cincinnati Ballet (1555 Central Parkway; 513-621-5219; www.cincinnatiballet.com) might indicate otherwise, but don’t be fooled: the Ballet’s Open Adult Division program is a great place to get lean. Start your Saturday with a beginning ballet class (90 minutes, $14), as a company member steers novices through basic movements. More experienced dancers might try the one-hour Rhythm and Motion class, which combines hip-hop, modern and African dance. Regulars know the moves, so pick a spot in the back and prepare to sweat.

11:30 a.m.
5) A BRIDGE TO BRUNCH

If John Roebling’s Suspension Bridge looks familiar, you might be thinking of his more famous design in New York. (Cincinnati’s version opened in 1867, almost two decades before the Brooklyn Bridge.) The pedestrian-friendly span over the Ohio River provides terrific views of the skyline. Cross into Covington, Ky., and walk about two blocks to Greenup Café (308 Greenup Street; 859-261-3663; greenupcafe.com), a homey outpost that offers a hearty brunch in vibrant parlor rooms. Traditional dishes like eggs Benedict ($9.75) and quiche Lorraine ($8.75) are expertly rendered; try a side of goetta ($3), a mixture of ground pork and oats brought to Cincinnati by German immigrants.

2 p.m.
6) TRACING A LEGACY

The National Underground Railroad Freedom Center (50 East Freedom Way; 513-333-7500; www.freedomcenter.org; adult ticket, $9 until the end of summer) is a dynamic testament to Cincinnati’s place in the antislavery movement. Multimedia presentations, art displays and interactive timelines trace the history of the global slave trade as well as 21st-century human trafficking. Leave time for the genealogy center, where volunteers assist individuals with detailed family searches.

4 p.m.
7) REVISITING NEWPORT VICE

For decades, Cincinnatians scoffed at their Kentucky neighbors, but that has been changing in the last few years with the revitalization of Newport’s waterfront and historic housing district. Still, relics of the town’s vice-filled past remain. For a taste, head to Sin City (822-824 Monmouth Street; 859-291-8486; www.sincityantiques.com), an antiques store that features grainy black-and-white stills of police raids dating from Newport’s heyday as the “other Las Vegas.” Inside, 27 vendors sell Victorian to mid-20th-century collectibles. York St Café (738 York Street, entrance on Eighth Street; 859-261-9675; www.yorkstonline.com), an 1880s-era apothecary, has been transformed into a three-story restaurant, music and art space, where wood shelves are stocked with kitschy memorabilia. Bistro fare includes the Mediterranean Board (an array of shareable appetizers; $18) and a delicate fresh halibut with spinach and artichoke ($23). Leave room for the excellent homemade desserts, including the strawberry buttermilk cake ($5).

7 p.m.
8) STAGE TO STAGE

Cincinnati Playhouse in the Park (962 Mount Adams Circle; 513-421-3888; www.cincyplay.com), which won a 2004 Tony Award for best regional theater and celebrates its 50th anniversary next season, offers splendid vistas of Mount Adams and a solid theatergoing experience. A lesser-known but equally engaging option can be found at the University of Cincinnati College-Conservatory of Music (Corry Boulevard; 513-556-4183; www.ccm.uc.edu). Students dreaming of Lincoln Center perform in full-scale productions like “Batboy” and “The Barber of Seville.” Though main stage tickets are $26 to $28, studio shows are free; check the online calendar for showtimes and locations.

10 p.m.
9) BALLROOM BLISS

Head back to Newport’s Third Street and its bars and clubs — the best of which, Southgate House, is set in an 1814 Victorian mansion that resembles a haunted fraternity (24 East Third Street, Newport; 859-431-2201; www.southgatehouse.com). It hosts local and national acts dabbling in everything from bluegrass to death metal. On a typical Saturday night, music fans of all ages and sensibilities roam the three venues: an intimate parlor room, a laid-back lounge and a ballroom with a capacity of 600. There is no cover charge for lounge shows; tickets for parlor and ballroom shows are usually $5 to $25.

Sunday

9 a.m.
10) REBIRTH OF NEIGHBORHOOD

As the epicenter of 19th-century German immigrant society, the neighborhood known as Over-the-Rhine once teemed with breweries, theaters and social halls. Though the area fell into disrepair, and parts remain rough around the edges, an $80 million revitalization effort has slowly brought back visitors. Walk down Main Street between 12th and 15th Streets for local artists’ galleries and the Iris BookCafe (1331 Main Street; 513-381-2665), a serene rare-book shop with an outdoor sculpture garden. A few blocks away, Vine Street between Central Parkway and 13th Street offers new boutiques including the craft shop MiCA 12/v (1201 Vine Street; 513-421-3500; www.shopmica.com), which specializes in contemporary designers like Jonathan Adler and Kenneth Wingard.

1 p.m.
11) DESIGNS TO BRING HOME

Before heading home, find inspiring décor at HighStreet (1401 Reading Road; 513-723-1901; www.highstreetcincinnati.com), a spacious and sleek design store. The owners have carefully composed a cosmopolitan mix of textiles, clothing and jewelry by New York and London designers as well as local artists, showcased in a creatively appointed space. A free cup of freshly brewed red flower tea makes it all the more inviting.

THE BASICS

Delta and Continental have nonstop flights from Newark Liberty International Airport, starting at $277, according to a recent online search, and Delta has a nonstop flight from La Guardia, starting at $341. Downtown Cincinnati is a 25-minute drive from the Cincinnati-Northern Kentucky International Airport. A rental car is recommended; parking is available on the street and at numerous garages.

The Hilton Cincinnati Netherland Plaza (35 West Fifth Street; 513-421-9100; www.hilton.com), a national historic landmark, is in the Art Deco Carew Tower. It has 561 renovated guest rooms; standard rate for a double room starts at $129.

The Westin Cincinnati (21 East Fifth Street; 513-621-7700; www.starwoodhotels.com) is right in the downtown area; many rooms feature views of the newly restored Fountain Square; rates begin at $129.
